#!/usr/bin/env sh

# -a <alias>        Alias to remove

while getopts ":a:" option; do
    case $option in
        a)
            alias=$OPTARG >&2
            ;;
        \?)
            echo "invalid option -$OPTARG"
            exit 1
            ;;
    esac
done

if [ -z "$alias" ]; then
    echo "Missing MinIO config alias"
    exit 127
fi

minio_client=$PGS3BAK_MINIO_CLIENT
[ -z "$minio_client" ] && minio_client=mc

$minio_client alias remove "$alias" --quiet > /dev/null
