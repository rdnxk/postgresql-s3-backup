#!/usr/bin/env sh

# -u <url>          URL of S3 instance
# -k <access key>   Access key
# -s <secret key>   Secret key

while getopts ":u:k:s:" option; do
    case $option in
        u)
            url=$OPTARG >&2
            ;;
        k)
            access_key=$OPTARG >&2
            ;;
        s)
            secret_key=$OPTARG >&2
            ;;
        \?)
            echo "invalid option -$OPTARG"
            exit 1
            ;;
    esac
done

if [ -z "$url" ]; then
    echo "Missing URL for S3 instance"
    exit 127
fi
if [ -z "$access_key" ]; then
    echo "Missing Access Key for S3 instance"
    exit 127
fi
if [ -z "$secret_key" ]; then
    echo "Missing Secret Key for S3 instance"
    exit 127
fi

name="pgs3bak-$(uuidgen --time | tr -cd '[:alnum:]')"

minio_client=$PGS3BAK_MINIO_CLIENT
[ -z "$minio_client" ] && minio_client=mc

$minio_client alias set "$name" "$url" "$access_key" "$secret_key" --quiet > /dev/null

## Return name so it can be removed later
echo "$name"
