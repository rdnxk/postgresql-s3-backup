#!/usr/bin/env sh

## Required
# -a <alias>            Use this named alias
# -b <bucket name>      Upload to bucket name
# -f <file>             File to upload

## Optional
# -o <object name>      Name of the object to be stored (if not set, it will use the -f)
# -c                    Create bucket if not exists

while getopts ":a:b:f:o:c" option; do
    case $option in
        a)
            alias=$OPTARG >&2
            ;;
        b)
            bucket_name=$OPTARG >&2
            ;;
        f)
            file=$OPTARG >&2
            ;;
        o)
            object_name=$OPTARG >&2
            ;;
        c)
            create_bucket_if_not_exists=1
            ;;
        \?)
            echo "invalid option -$OPTARG"
            exit 1
            ;;
    esac
done
if [ -z "$alias" ]; then
    echo "Missing alias"
    exit 127
fi
if [ -z "$bucket_name" ]; then
    echo "Missing bucket name"
    exit 127
fi
if [ -z "$file" ]; then
    echo "Missing file name"
    exit 127
fi
[ -z "$object_name" ] && object_name=$(echo "$file" | rev | cut -d/ -f1 | rev)

minio_client=$PGS3BAK_MINIO_CLIENT
[ -z "$minio_client" ] && minio_client=mc

[ -n "$create_bucket_if_not_exists" ] && [ "$create_bucket_if_not_exists" -eq 1 ] && $minio_client mb --ignore-existing "${alias}/$bucket_name"

$minio_client cp "$file" "${alias}/${bucket_name}/$object_name"
