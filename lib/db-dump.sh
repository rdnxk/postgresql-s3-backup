#!/usr/bin/env sh

## Required
# -d <db name>      Database to dump

## Optional
# -f <file>         Output to specified path (default /tmp/<db name>_<timestamp>.<format>)
###                 If path is a directory, the file will be generated inside of it with default naming strategy
# -F <format>       Export format of the dump (pg_dump --format)
# -z <level>        DB compression level (1-9) (for format=plain only) (pg_dump --compression)
# -O                Export database with no owner (pg_dump --no-owner)
# -j <jobs>         Number of parallel jobs for dumping (for format=directory only) (pg_dump --jobs)

## DB Connection
# -h <host>         Host to connect
# -p <port>         Host port to connect
# -U <username>     Username to connect
# -W <password>     Password to connect (if unset, it will use .pgpass file)

## If set, some of these variables will override the PostgreSQL environment variables like PGOPTIONS, PGUSER, PGHOST, PGPORT, PGDATABASE etc

while getopts ":d:F:f:z:Oj:h:p:U:W:" option; do
    case $option in
        ## Required
        d)
            db=$OPTARG >&2
            ;;
        ## Optional
        f)
            file=$OPTARG >&2
            ;;
        F)
            format=$OPTARG >&2
            ;;
        z)
            compression=$OPTARG >&2
            ;;
        O)
            owner=0
            ;;
        j)
            jobs=$OPTARG >&2
            ;;
        ## DB Connection
        h)
            host=$OPTARG >&2
            ;;
        p)
            port=$OPTARG >&2
            ;;
        U)
            username=$OPTARG >&2
            ;;
        W)
            password=$OPTARG >&2
            unset pgpass
            ;;
        \?)
            echo "invalid option -$OPTARG"
            exit 1
            ;;
    esac
done
if [ -z "$db" ]; then
    echo "Missing database name"
    exit 127
fi
# shellcheck disable=SC2089
opts="--dbname \"$db\""

[ -n "$format" ] && opts="$opts --format \"$format\""
[ -n "$compression" ] && opts="$opts --compress \"$compression\""
[ "$owner" -eq 0 ] && opts="$opts --no-owner"
[ -n "$jobs" ] && opts="$opts --jobs \"$jobs\""
[ -n "$host" ] && opts="$opts --host \"$host\""
[ -n "$port" ] && opts="$opts --port \"$port\""
[ -n "$username" ] && opts="$opts --username \"$username\""
if [ -n "$password" ] && [ -n "$host" ] && [ -n "$port" ] && [ -n "$username" ]; then
    # shellcheck disable=SC2153
    pgpassfile="$PGPASSFILE"
    [ -z "$pgpassfile" ] && pgpassfile=$HOME/.pgpass
    line="${host}:${port}:${db}:${username}:$password"
    echo "$line" >> "$pgpassfile"
fi

if [ -d "$file" ]; then
    parent_dir="$file"
    unset file
fi
if [ -z "$file" ]; then
    if [ "$format" = "p" ] || [ "$format" = "plain" ]; then
        ext="sql"
    elif [ "$format" = "d" ] || [ "$format" = "directory" ]; then
        ext="d"
    elif [ "$format" = "t" ] || [ "$format" = "tar" ]; then
        ext="tar"
    elif [ "$format" = "c" ] || [ "$format" = "custom" ]; then
        ext="pgsql"
    fi
    [ -n "$compression" ] && ext="${ext}.gz"
    timestamp=$(date --utc +%Y-%m-%dT%H:%M:%SZ)
    file="${db}_${timestamp}.$ext"
    [ -n "$parent_dir" ] && file="$parent_dir/$file"
fi
opts="$opts --file \"$file\""

echo "$opts" | xargs pg_dump

## TEARDOWN
## remove line from .pgpass
[ -n "$line" ] && [ -n "$pgpassfile" ] && sed "/$line/d" "$pgpassfile"

## return file name so it can be passed to uploader and removed later
echo "$file"
