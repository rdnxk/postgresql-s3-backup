#!/usr/bin/env sh

for configfile in "/etc/pgs3bak/pgs3bak.conf" "$HOME/.pgs3bak" "$HOME/.config/pgs3bak/pgs3bak.conf"; do
    [ -r "$configfile" ] && . "$configfile"
done

LIB_DB_DUMP=$PGS3BAK_LIB_PATH/db-dump.sh
LIB_MINIO_CONFIG_CREATE=$PGS3BAK_LIB_PATH/minio-config-create.sh
LIB_MINIO_CONFIG_REMOVE=$PGS3BAK_LIB_PATH/minio-config-remove.sh
LIB_ROTATE_OLD_BACKUPS=$PGS3BAK_LIB_PATH/rotate-old-backups.sh
LIB_UPLOAD_TO_S3=$PGS3BAK_LIB_PATH/upload-to-s3.sh

## DUMP VARIABLES
db="$PGS3BAK_DATABASE_NAME"
file="$PGS3BAK_OUTPUT_FILE"
format="$PGS3BAK_OUTPUT_FORMAT"
compression="$PGS3BAK_OUTPUT_COMPRESSION"
no_owner="$PGS3BAK_NO_OWNER"
jobs="$PGS3BAK_JOBS"

## MINIO VARIABLES
alias="$PGS3BAK_MINIO_ALIAS"
url="$PGS3BAK_MINIO_URL"
access_key="$PGS3BAK_MINIO_ACCESS_KEY"
secret_key="$PGS3BAK_MINIO_SECRET_KEY"
bucket="$PGS3BAK_MINIO_BUCKET"
# @TODO
create_bucket_if_not_exists="$PGS3BAK_MINIO_CREATE_BUCKET_IF_NOT_EXISTS"
object_name="$PGS3BAK_MINIO_OBJECT_NAME"
older_than="$PGS3BAK_ROTATE_OLDER_THAN"

while getopts ":vd:f:F:z:Oj:a:u:k:s:b:o:t:h:p:U:W:" option; do
    case $option in
        v)
            echo "version 0.1.0"
            exit 0
            ;;
        ## PostgreSQL vars
        d)
            db=$OPTARG >&2
            ;;
        f)
            file=$OPTARG >&2
            ;;
        F)
            format=$OPTARG >&2
            ;;
        z)
            compression=$OPTARG >&2
            ;;
        O)
            no_owner=1
            ;;
        j)
            jobs=$OPTARG >&2
            ;;
        ## MinIO vars
        a)
            alias=$OPTARG >&2
            ;;
        u)
            url=$OPTARG >&2
            ;;
        k)
            access_key=$OPTARG >&2
            ;;
        s)
            secret_key=$OPTARG >&2
            ;;
        b)
            bucket=$OPTARG >&2
            ;;
        o)
            object_name=$OPTARG >&2
            ;;
        t)
            older_than=$OPTARG >&2
            ;;
        ## DB Connection
        h)
            host=$OPTARG >&2
            ;;
        p)
            port=$OPTARG >&2
            ;;
        U)
            username=$OPTARG >&2
            ;;
        W)
            password=$OPTARG >&2
            unset pgpass
            ;;
        \?)
            echo "invalid option -$OPTARG"
            exit 1
            ;;
    esac
done

## Dump
dump_opts=""
[ -n "$file" ] && dump_opts="$dump_opts -f \"$file\""
[ -n "$format" ] && dump_opts="$dump_opts -F \"$format\""
[ -n "$compression" ] && dump_opts="$dump_opts -z \"$compression\""
[ -n "$no_owner" ] && dump_opts="$dump_opts -O"
[ -n "$jobs" ] && dump_opts="$dump_opts -j \"$jobs\""
[ -n "$host" ] && dump_opts="$dump_opts -h \"$host\""
[ -n "$port" ] && dump_opts="$dump_opts -p \"$port\""
[ -n "$username" ] && dump_opts="$dump_opts -U \"$username\""
[ -n "$password" ] && dump_opts="$dump_opts -W \"$password\""
[ -z "$db" ] && db=$PGDATABASE
db_file=$(echo "$dump_opts" | xargs "$LIB_DB_DUMP" -d "$db")

## Get MinIO Config
if [ -z "$alias" ]; then
    alias=$($LIB_MINIO_CONFIG_CREATE -u "$url" -k "$access_key" -s "$secret_key")
    remove_alias=1
fi

## Upload to S3
upload_opts=""
[ -n "$object_name" ] && upload_opts="-o \"$object_name\""
[ -n "$create_bucket_if_not_exists" ] && [ "$create_bucket_if_not_exists" -eq 1 ] && upload_opts="$upload_opts -c"
echo "$upload_opts" | xargs "$LIB_UPLOAD_TO_S3" -a "$alias" -b "$bucket" -f "$db_file"

## Remove old backups from S3
[ -n "$older_than" ] && [ "$older_than" -ne 0 ] && $LIB_ROTATE_OLD_BACKUPS -a "$alias" -b "$bucket" -t "$older_than"

## teardown
if [ -n "$PGS3BAK_KEEP_LOCAL_DUMP" ] || [ "$PGS3BAK_KEEP_LOCAL_DUMP" != "1" ]; then
    rm -fr "$db_file"
fi

[ -n "$remove_alias" ] && $LIB_MINIO_CONFIG_REMOVE -a "$alias"
