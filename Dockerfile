FROM alpine:latest AS builder

RUN mkdir -p /usr/lib/pgs3bak/ /opt/pgs3bak

WORKDIR /opt/pgs3bak/
COPY . .

RUN mv bin/pgs3bak /usr/bin/pgs3bak && \
    chmod +x /usr/bin/pgs3bak && \
    mkdir -p /etc/pgs3bak && \
    mv lib/* /usr/lib/pgs3bak/

## Production
ARG PG_VERSION=14
FROM alpine:latest

COPY --from=builder /usr/bin/pgs3bak /etc/pgs3bak/ /usr/lib/pgs3bak /

RUN apk upgrade --update --no-cache && \
    ## install uuidgen and postgresql-client (that includes pg_dump)
    apk add --no-cache uuidgen "postgresql$PG_VERSION-client" && \
    ## install minio-client
    wget -q https://dl.min.io/client/mc/release/linux-amd64/mc && \
    chmod +x mc && \
    mv mc /usr/bin/mc

CMD ["pgs3bak"]
