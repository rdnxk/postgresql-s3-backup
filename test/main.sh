#!/usr/bin/env sh

test_dir=$(dirname "$(realpath "$0")")

. "$test_dir/hooks.sh"

for test_suite in "$test_dir/unit/"*; do
    setup 100
    sh "$test_suite"
    exit_code=$?
    echo "$test_suite done with exit code $exit_code"
    teardown
    [ "$exit_code" -ne 0 ] && exit $exit_code
done

echo "Tests finished successfully"
