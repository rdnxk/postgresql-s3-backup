#!/usr/bin/env sh

PGS3BAK_LIB_PATH=$PWD/lib \
PGHOST=localhost \
PGPORT=5432 \
PGUSER=pgs3bak_user \
PGPASSWORD=pgs3bak_password \
PGDATABASE=pgs3bak \
PGS3BAK_MINIO_URL=http://localhost:9000 \
PGS3BAK_MINIO_ACCESS_KEY=pgs3bak_s3_user \
PGS3BAK_MINIO_SECRET_KEY=pgs3bak_s3_password \
PGS3BAK_MINIO_BUCKET=my-bucket \
PGS3BAK_MINIO_CLIENT=mcli \
sh test/main.sh
